#!/bin/sh

# Checks arguments
if [ $# -ne 2 ]
then
    echo "Wrong number of arguments (expected 2, given "$#")\nUsage: " $0 "<data-dir> <index-dir>"
    exit 1
fi

# Builds and runs a C++ module
python data-mine.py $@

