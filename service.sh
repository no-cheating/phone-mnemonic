#!/bin/sh

# Checks arguments
if [ $# -lt 2 ]
then
    echo "Wrong number of arguments (expected at least 2, given "$#")\nUsage: " $0 "<index-dir> <list-of-telephone-numbers>"
    exit 1
fi

# Builds and runs a C++ module
python service.py $@