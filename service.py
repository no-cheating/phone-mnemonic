from sys import argv

import shared


def readIndex(indexFilePath):
    index = {}

    with open(indexFilePath, "r") as indexFile:
        for line in indexFile:
            # Trims trailing new line character
            if line.endswith("\n"):
                line = line[:-1]

            indexRecord = line.split(shared.INDEX_FILE_SEPARATOR)
            number      = indexRecord[0]
            wordsList   = [ wordRecord.split(shared.INDEX_FILE_SUBSEPARATOR) \
                            for wordRecord in indexRecord[1:] ]
            index[number] = wordsList

    return index


def checkMnemonics(numbers, index):
    for number in numbers:
        resultToPrint = str(number) + ":"
        if number in index:
            numberRecord  = index[number]
            first         = True

            for wordRecord in numberRecord:
                if first:
                    first = False
                else:
                    resultToPrint += ","
                resultToPrint += " " + wordRecord[0] + \
                                 " (" + wordRecord[1] + ")"
        
        print(resultToPrint + "\n")


def main():
    indexDir = argv[1]
    numbers  = argv[2:]

    index = readIndex(indexDir + shared.INDEX_FILE_NAME)
    ## TOREMOVE: Debug information
    # for number in index:
    #     print(str(number) + ": " + str(index[number]))
    ##
    checkMnemonics(numbers, index)


if __name__ == "__main__":
    main()
