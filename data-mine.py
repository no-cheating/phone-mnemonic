from os        import walk
from sys       import argv
from math      import log
from re        import split
from functools import partial

import shared


### Constants
WORD_LEN_MIN    = 3
WORD_LEN_MAX    = 12
WORD_SEPARATORS = "[ \n\.]+"    # Must be in form of regular expression
WORD_MIN_COUNT  = 3


# Returns a digit corresponding to the given character
def charToDigit(char):
    if char in [ 'a', 'b', 'c' ]:
        return 2
    elif char in [ 'd', 'e', 'f' ]:
        return 3
    elif char in [ 'g', 'h', 'i' ]:
        return 4
    elif char in [ 'j', 'k', 'l' ]:
        return 5
    elif char in [ 'm', 'n', 'o' ]:
        return 6
    elif char in [ 'p', 'q', 'r', 's' ]:
        return 7
    elif char in [ 't', 'u', 'v' ]:
        return 8
    elif char in [ 'w', 'x', 'y', 'z' ]:
        return 9
    else:
        raise RuntimeError("Not supported character: " + char)


# Returns a word corresponding to the given word
def wordToNumber(word):
    numberStr = []
    for char in word:
        numberStr.append(str(charToDigit(char)))
    return int("".join(numberStr))


# Loads all the words from the given file and adds them to the newly created
# index. Returns that index.
def processFile(fileName):
    index      = {}
    totalWords = 0

    file      = open(fileName, "r")
    fileLines = file.readlines()

    for line in fileLines:
        # words list may contain empty strings, but we don't care as they
        # will be discarded later when checking against minimum word length
        words = split(WORD_SEPARATORS, line)

        for word in words:
            totalWords += 1
            word = word.lower()
            if WORD_LEN_MIN <= len(word) <= WORD_LEN_MAX:
                if word in index:
                    index[word] += 1
                else:
                    index[word] = 1

    return { "index": index, "totalWords": totalWords }


# Builds and returns index containing information about all words in all of the
# files in 
def buildWordIndex(dataDir):
    wordIndex = {}
    totalDocs = 0

    for (path, _, childFiles) in walk(dataDir):
        for fileName in childFiles:
            if fileName.lower().endswith(".txt"):
                totalDocs += 1
                ## TOREMOVE: Debug information
                if (totalDocs % 750 == 0):
                    print str(totalDocs / 750) + "%"
                ##
                currentFileIndexObj = processFile(path + fileName)
                currentFileIndex = currentFileIndexObj["index"]
                currentFileWords = currentFileIndexObj["totalWords"]

                # Update global index and calculate tf-idf for words from
                # current file
                for word in currentFileIndex:
                    # Update global index
                    wordOccurences = currentFileIndex[word]
                    tfIdfFactor    = float(wordOccurences) / currentFileWords
        
                    if word in wordIndex:
                        wordRecord = wordIndex[word]
                        wordRecord["docs"]  += 1
                        if (wordRecord["maxTfIdfFactor"] < tfIdfFactor):
                            wordRecord["maxTfIdfFactor"] = tfIdfFactor
                    else:
                        wordIndex[word] = {
                            "docs"           : 1,
                            "maxTfIdfFactor" : tfIdfFactor
                        }
            # ## TOREMOVE: Processes only first file
            # break
            # ##

    return { "index": wordIndex, "totalDocs": totalDocs }


# Builds and returns index mapping numbers to words
def buildNumberIndex(wordIndex, totalDocs):
    numberIndex = {}

    for word in wordIndex:
        wordRecord = wordIndex[word]
        tfIdf      = wordRecord["maxTfIdfFactor"] * log(float(totalDocs) / (1 + wordRecord["docs"]))
        number     = wordToNumber(word)

        # Update number index
        if number in numberIndex:
            insertWordToList(word, tfIdf, numberIndex[number])
        else:
            numberIndex[number] = [ [ word, tfIdf ] ]

    return numberIndex


# Insert given word with it's tf-idf score to list of other words and it's
# scores, so that the list remains sorted by the descending tf-idf score.
# Uses binary search for finding the insertion position.
def insertWordToList(word, tfIdf, wordsList):
    start = 0
    end   = len(wordsList)

    # Finding insertion position
    while (start < end):
        center = (end + start) / 2
        if tfIdf < wordsList[center][1]:
            start = center + 1
        else:
            end = center

    wordsList.insert(start, [ word, tfIdf ])


# Writes the whole index to a specified file
def writeIndex(indexFilePath, numberIndex):
    indexFile = open(indexFilePath, "w")

    for number in numberIndex:
        numberRecord = numberIndex[number]
        indexLine    = str(number)
        for wordRecord in numberRecord:
            indexLine += shared.INDEX_FILE_SEPARATOR + wordRecord[0] + \
                         shared.INDEX_FILE_SUBSEPARATOR + str(wordRecord[1])
        indexFile.write(indexLine + "\n")

    indexFile.close()


def main():
    dataDir  = argv[1]
    indexDir = argv[2]
    
    wordIndexObj = buildWordIndex(dataDir)
    wordIndex    = wordIndexObj["index"]
    totalDocs    = wordIndexObj["totalDocs"]
    numberIndex  = buildNumberIndex(wordIndex, totalDocs)

    writeIndex(indexDir + "/" + shared.INDEX_FILE_NAME, numberIndex)

    # ## TOREMOVE: Debug information
    # print "\nWORD INDEX"
    # for word in wordIndex:
    #     print(word + ": " + str(wordIndex[word]))
    # print "\nNUMBER INDEX:"
    # for number in numberIndex:
    #     print(str(number) + ": " + str(numberIndex[number]))
    # ##


if __name__ == "__main__":
    main()
