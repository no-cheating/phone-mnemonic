# Phone Mnemonic

## Problem statement

Dr. Thomas "Tummy" Slicer is in charge of organizing a medical conference
this year. He would like to make the conference more attractive to attendees.
After some hard thinking he comes up with a brilliant idea. Once he was late
for a flight, and when looked at his plane ticket, he noticed that the British
Airways service number is given as 1-800-AIRWAYS. How cool it would be, he
thought, to offer registrants a free service which looks for mnemonics for
their phone numbers. It would be even cooler if these mnemonics somehow had a
medical connection too! Cognizant of the lack of his technical abilities,
post haste he hires an engineer, you! Having blown most of his budget for the
conference, Dr. Slicer can only afford three days of your consultancy.

This is his description of the problem:

> We need a service which takes in telephone numbers and tries to find
   mnemonics for those numbers. The numbers will be between 3 and 12
   digits. For example, if someone typed in 3473273, a matching
   mnemonic would be "disease". On top of that, I'd prefer if those
   mnemonics were medical terms as much as possible. Unfortunately,
   I can only afford 3 days of your time, but I pay you nothing if your
   program does not work. On the other hand, I pay you a day worth of
   bonus if you go the extra mile and come up with something really clever!
   Of course, finding such mnemonics should be fast, since it will be
   part of the on-line registration, and I want them to be able to search
   for as many phone numbers as they wish. We expect to have over two thousand
   registrants this year!

> The character-digit correspondence is printed on most telephones. Here it is
   for reference:

    a,b,c -> 2
    d,e,f -> 3
    g,h,i -> 4
    j,k,l -> 5
    m,n,o -> 6
    p,q,r,s -> 7
    t,u,v -> 8
    w,x,y,z -> 9

## Your approach to the problem

You agree to take his offer. After some thinking, you arrive at the following
design. PubMed has a collection of close to 75K medical articles. You will
use the words in those articles as candidate mnemonics. You also know from your
Artificial Intelligence studies that there is a well known approach to assign
importance to words in documents. It is called the TF-IDF score:

    tf-idf(w,d) =   f(w)/W(d) * ln (|D| / (1 + c(w,D))

where

    w:           a particular word
    d:           a particular document
    tf-idf(w,d): the importance of word w in document d
    f(w):        the number of occurrences of w in d
    W(d):        the total number of words (counting both duplicates and
                 words that do not qualify as mnemonics) in d
    ln:          natural logarithm function
    |D|:         the total number of documents
    c(w,D):      the number of documents containing the word w

Basically, the `tf` part promotes frequently occurring words, while the `idf` part
demotes those words that are common in all documents.

You reason that words that receive high tf-idf scores are likely to be more
"medical" and thus more relevant. Note that a word (e.g. aspirin) will have
different tf-idf scores in each different document it appears in. You will
assign the highest of these scores to the word. An artifact of this scoring
technique is that it promotes typos. Very rare words will have a very high
idf score. Thus you will discard words that do not occur at least in 3
different documents (that is, c(w,D) >= 3).

You are also worried (rightfully) that these 2000+ people will register in a
short time window and some of them will try a lot of phone numbers. Thus you
decide to separate the problem into a Data Mining part and to a Servicing
part. During the Data Mining part, you will build something (models, indexes,
etc ...) that will allow you to search for the numbers really fast. The Data
Mining part will be performed off-line, therefore it can take some (reasonable)
time. You are still cautious, and aware that you need to process almost a
75K articles. In the Servicing phase, you will take phone numbers and
print the corresponding mnemonics in tf-idf score order. Think of the Service
part as an actual live service, so a short warm-up time is fine.

## Your solution

* Feel free to use any reasonably "common" language (Java, C/C++, Python, JavaScript, etc.)
* Your solution should be submitted here, on Bitbucket. Fork this repository,
solve the assignment (commit early and often!), and invite us to
your repo (andrascsibi, phraktle, dfogaras).
* Your program must run on Linux, do discuss with us if you need some
funky tools installed. You can assume Java 8, gcc-4.8, Python 2.7. Do discuss also if you want some "tool" that cannot be considered
a programming language or a utility library
* Please provide a build script (make, ant, specific shell script, etc) that will
build your program from source (for non-interpreted languages, of course)
* You may not use a database (such as MySQL, Mongo, Lucene, etc ...), sorry!
We do want to see how you can code and manage larger data structures
* There must be two entry points to your system:

    1. Invoking your data miner: it will be given two arguments.
The first one is the path of the directory, where ALL files
with the .txt extension need to be processed (other entries should
be ignored). The second argument is the path to a directory, where
you can build whatever you want; we shall call this index-directory.

    2. Invoking your service: it will be given as first argument the
index-directory (see above) and an arbitrary number of phone numbers.
It should print mnemonics for each phone number in tf-idf scoring
order (with the scores also displayed)!

We do ask you to wrap these entry points in shell scripts; inside you
can perform any parameterization, etc ...

Example:

    $ ./data-mine.sh /usr/local/repo /var/tmp/index
    # any output, but please not much! there will be 75K docs in
    # /usr/local/repo

    $ ./service.sh /var/tmp/index 72846436 72840436 2774746 2774716 12003 987464 2639934448
    72846436: ratingen (0.0638998), pathogen (0.0543559)
    72840436: path0gen (0.0543559)
    2774746: aspirin (0.199684)
    2774716: aspir1n (0.199684)
    12003:
    987464: yuping (0.0053198), yuqing (0.00294109), xuping (0.00274428)
    2639934448: body-weight (0.0790547), bodyweight (0.0466669)
    # this should be really fast, of course we will use a different
    # list to test :)
    # note that for 72840436 and 2774716 you get completions only if
    # you have implemented bonus 1, and the multi-word completion
    # for 2639934448 requires bonus 2 (see below)
    # the parentheses enclose the tf-idf score of the word

You will be given the "cleaned" PubMed database in an archive consisting of
73,909 text files. These text files only contain the characters 'A'-'Z',
'a'-'z', ' ', '.', and '\n'. You can absolutely rely on this! There are no
numbers, accented or binary characters, tabs, etc ... We have done this
much preprocessing for you.

The PubMed archive is available for download here:

> http://download.scarabresearch.com/recruitment/txt.tgz

## Bonuses

   Important! Bonuses will only be considered if you could solve the previous
   problem. We suggest you do not start with these! These bonuses may be
   harder than they appear at first glance!

   You notice that even with these many documents you hardly get any hits.
   You have a number of ideas to improve!

### Bonus 1

You notice that the digits 1 and 0 have no corresponding letters!
Considering independent distribution of digits, a 7 digit phone
number has an ~ 80% chance of containing at least one 0 or 1;
thus cannot be converted into a mnemonic regardless of the corpus!
You decide to map these by completing 0 to "o" and 1 to "i", but
will print the original numbers (i.e. 0 or 1) in the mnemonic.
For example, 2771716 will become "asp1r1n", and 72840436 maps
to "path0gen".

### Bonus 2

You decide to offer multi-word mnemonics! You will only consider
a sequence of words if this very sequence also occurs somewhere
in the corpus. You also mark the boundaries in the printout with
a dash. For example, 2639934448 -> "body-weight". If the words
"body" and "weight" occur separately but never in this order
side-by-side, then there is no such completion. A period (.)
will terminate such sequences. That is "... body. Weight ..."
is not considered adjacent. You do not consider words less than
3 letters long. The tf-idf score of a multi-word mnemonic is
the average of the comprising words and at least one of the words
must occur in 3 different documents

### Bonus 3

75K documents is a hefty sum. Modern computers have several
cores, and in particular we will run your program on a
4 core machine. If you can use this to speed up processing,
this will be considered great! You must tell us how to set
the number of cores you want to use, and you only get the bonus
if your data miner runs faster on 4 cores than on 1!

### Bonus 4

You figure, the more documents the better, so you may be able to
get corpora from other sources. But you do not want to do the
data mining part from scratch! Your data miner will take
into account already existing structures in the index directory.

Thus if we run

    $./data-mine.sh /usr/local/repo1 /var/tmp/index
    $./data-mine.sh /usr/local/repo2 /var/tmp/index

in this sequence and `/usr/local/repo1` contained one part of
these documents and `/usr/local/repo2` the other, it would be
the same in effect as if we just called

    $./data-mine.sh /usr/local/repo /var/tmp/index

where `/usr/local/repo` contains all those documents