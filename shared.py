INDEX_FILE_NAME         = "index.txt"

# Both separators must be characters not appearing in data files
INDEX_FILE_SEPARATOR    = ";"
INDEX_FILE_SUBSEPARATOR = ","
